

			------- ABOUT V8 JAVASCRIPT ENGINE  -------


	1.v8 is google open source high-performance javascript and webAssembly engine,written in c++.

	2.it is used in chrome and in node.js among others.

	3.This documentation is aimed at c++ developers who want to use v8 in their applications,aswellas
	  anyone interested in v8 design and performance.

	4.This documentation introduces you to v8,while the remaining documentation shows you how tu us v8 
	  in your code and describes some of its design details..

	5.As well as providing a set if javascript benchmarks for measuring v8 performance...

	6.V8 compiles and executes JavaScript source code, handles memory allocation for objects, and garbage 
	  collects objects it no longer needs.

	7. V8’s stop-the-world, generational, accurate garbage collector is one of the keys to V8’s performance.

	8.JavaScript is commonly used for client-side scripting in a browser, being used to manipulate DOM.

	9.The DOM is not, however, typically provided by the JavaScript engine but instead by a browser. 

	10.The same is true of V8 — Google Chrome provides the DOM.

	11.V8 does however provide all the data types, operators, objects and functions specified in the ECMA standard.

	12.V8 enables any C++ application to expose its own objects and functions to JavaScript code.

	13.t’s up to you to decide on the objects and functions you would like to expose to JavaScript.

	